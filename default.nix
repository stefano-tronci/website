with import <nixpkgs> { };

let
  pythonPackages = python3Packages;
in pkgs.mkShell rec {
  name = "impurePythonEnv";
  venvDir = "./.venv";
  buildInputs = [
    # The chosen python interpreter
    python311Packages.python

    # venvShellHook to create a virtual env.
    python311Packages.venvShellHook

    # Dependencies from nixpkgs (will be added to PYTHONPATH).
    python311Packages.wheel
    python311Packages.setuptools
    pkgs.python311Packages.jupyter
    pkgs.python311Packages.notebook
    pkgs.python311Packages.ipykernel
    pkgs.python311Packages.papermill
    pkgs.python311Packages.plotly
    pkgs.python311Packages.numpy

    # Dependencies to build binary extensions.
    taglib
    openssl
    git
    libxml2
    libxslt
    libzip
    zlib
    
    # Quarto and document utilities.
    quarto
    texliveFull
  ];

  # The commands below run only after creating the virtual environment. They install requirements if present.
  # They also install a local package (with symlinks) if present.
  postVenvCreation = ''
    unset SOURCE_DATE_EPOCH
    test -f requirements.txt && pip install -r requirements.txt || echo "File requirements.txt Does Not Exists - Skipping!"
    test -f pyproject.toml && pip install -e . || echo "File pyproject.toml Does Not Exists - Skipping!"
  '';

  # Now we can execute any commands within the virtual environment.
  # This is optional and can be left out to run pip manually.
  postShellHook = ''
    # allow pip to install wheels
    unset SOURCE_DATE_EPOCH
  '';
  # ls ${pkgs.python3.sitePackages}
  # ln -sf ${python-env}/lib/python3.11/site-packages/* ${venvDir}/lib/python3.11/site-packages
}
