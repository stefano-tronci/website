---
title: "REST API with Flask and Marshmallow-Dataclass"
author: "Stefano Tronci"
date: "2024-02-03"
categories: [code]
image: "featured.png"
---

![](featured.png)

Recently I have been implementing a bunch of different services with Python. These run on a server on a network. They are programmed to accept network requests and provide an answer. A service can be anything. For example, a large computation for an acoustic simulation performed on a remote, very powerful, machine. To implement this type of service so far I used [`Flask`](https://flask.palletsprojects.com/en/2.3.x/) and [`marshmallow_dataclass`](https://lovasoa.github.io/marshmallow_dataclass/html/marshmallow_dataclass.html). I found this topic somewhat hard to learn. Not so much for the complexity: modern Python tools are actually very straightforward to use. Rather, it is a really deep field with tons of things to learn. For this reason I put together a [little example project](https://gitlab.com/stefano-tronci/pysimserviceexample) to serve as a reference for me in the future. This is mainly a *"note to self"* article, but it might be interesting to read for you too!

# The Project

I called this project `pySimServiceExample`. The rather not creative name tells that:

-   The project is implemented with Python.
-   It is a service that runs simulations.
-   It is an example.

The project is full featured, in the sense that it has:

-   A fast simulation engine for a real acoustic simulation.
-   A server which offers a REST API.
-   Helpful Python client functions to ease the development of Python clients.
-   A [documentation](https://stefano-tronci.gitlab.io/pysimserviceexample/).

The documentation goes into [the details of how this project is designed and why](https://stefano-tronci.gitlab.io/pysimserviceexample/explanation/) which I invite you to read. Instead of repeating that paragraph over here I will go through the design paradigms and tools I used and sort of critique them.

# Error Handling

When I code projects of this kind I tend to write most functions to not raise exceptions and instead tell the caller if the operation was successful. To do so I used the [`result`](https://github.com/rustedpy/result) package in the example project.

I like this because it allows me to:

-   Not have the server application die whenever there is an error.
-   Provide useful error messages to users thought the REST API response.

However, I also hate this because it forces me to write a lot of code to trap possible errors. Sometimes this code makes use of [`isinstance`](https://docs.python.org/3/library/functions.html#isinstance) to check for types, which runs against the way Python itself is designed. I also end up using a lot of [`try`](https://docs.python.org/3/reference/compound_stmts.html#try) statements which tend to make the code less readable.

The project makes use of [`typing`](https://docs.python.org/3/library/typing.html) to produce useful data models and validation (more on this later). This helps tremendously in reducing the check and validation code to a minimum. However, I wonder whether it is best to write code normally, allowing exceptions to be raised, and then have a service able to restart itself if anything goes wrong (as in the [systemd service I provided as an example](https://stefano-tronci.gitlab.io/pysimserviceexample/how-to-guides/#how-to-create-a-systemd-service)). Then, additional checks would be implemented only when needed instead of becoming a tedious design paradigm.

I used the "non throwing exceptions" design paradigm a lot lately, but I have been wondering whether it should be better to drop it.

# Data Models

The application makes extensive use of [`dataclasses`](https://docs.python.org/3/library/dataclasses.html) to define, first and foremost, data models. An instance of a `dataclass` can be easily serialised to `JSON` by using [`marshmallow-dataclass`](https://pypi.org/project/marshmallow-dataclass/). The inverse process, from `JSON` to class instance, is also very straightforward. Data classes are then a very convenient way to implement a REST API. If the REST API is designed to accept a `JSON` body, or `JSON` files, then those can be validated and turned into Python class instances automatically!

However, I recently found out about [`pydantic`](https://docs.pydantic.dev/latest/). `pydantic` allows do to pretty much the same things. However, it appears to have more advanced features to me. I think I will be probably try out `pydantic` in future projects.

# `Flask`

[`Flask`](https://flask.palletsprojects.com/en/3.0.x/) is an amazing tool, and it is really very easy to implement a REST API with it. However, I recently found out about [`FastAPI`](https://fastapi.tiangolo.com/). `FastAPI` appears to make it even easier to create APIs. But, on top of that, it also has very interesting automatic documentation generation which I really like. I think I will be trying `FastAPI` for my next project of this kind.

# Conclusion

Overall, I think the design paradigms and packages/tools that I used so far to implement services and APIs are a very solid choice. However, by working on projects of this kind (including this example project) I learned that there are other tools out there that could be even better for this type of application. I will most likely be using them for my next projects.