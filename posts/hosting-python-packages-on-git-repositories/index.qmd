---
title: "Hosting Python Packages on Git Repositories"
author: "Stefano Tronci"
date: "2022-04-23"
categories: [code]
image: "https://www.python.org/static/community_logos/python-logo-generic.svg"
---

![](https://www.python.org/static/community_logos/python-logo-generic.svg)

Creating packages is a very nice way to reuse Python code. Packages allow easy installation of software through `pip`. They are often hosted on [PyPI](https://pypi.org/) but it is possible to host them on any [Git](https://git-scm.com/) repository. Packages hosted on Git repositories can be installed with `pip` just like those hosted on PyPI. I find this very convenient, especially when a project cannot be hosted on PyPI. This can happen if you are working for an organisation, for example. In this article I will illustrate how to host a package on a Git repository.

::: callout-note
# A Bit Outdated

The information in this article is a bit outdated as of mid 2023. Many things have changed in [setuptools](https://setuptools.pypa.io/en/latest/). The techniques presented in this article still work, but there will be an update version of this article soon.
:::

# Packaging

In this guide I will outline how to create a package step by step. I will start from an empty Git repository. This should also provide all the information needed to refactor an existing project into a package. **I will not cover how to host a package on PyPI**.

## Name Your Package

Your package will need to have a unique name among all the software sources users might have access to, including PyPI. This to prevent eventual conflicts. Give your package a memorable and unique name.

In this guide I will use `pyPackExample` as an example. This package is hosted on [GitLab](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags). Any Git host should work just the same.

## Git Repository Creation

Create an ordinary repository with the same name as the package (this is not necessary, but makes things more straightforward). This can be done in any way. For me the simplest way is to create the repository on the web interface of the hosting service (GitLab, GitHub...) complete with an initial `README` file. Then I clone it on the local machine.

From now on we will assume that:

-   A Git repository is setup;
-   A local copy of your repository is cloned on your local machine.

We call the repo `pypackexample`. At this point the internal folder structure of your local copy (excluding `.git/`) should look like this:

``` text
pypackexample/
└── README.md
```

::: callout-tip
The directory contents can be printed with the following command:

``` commandline
tree -a pypackexample/ -I .git
```
:::

## Adding a `.gitignore` File

When we build and install our package locally we might create a number of artefacts. It is best to add a `.gitignore` file to prevent these (and other files) from being added to the repository. Create the file in the root of the repo:

``` text
pypackexample/
├── .gitignore
└── README.md
```

A good starting point for the .gitignore contents is this:

``` text
# Virtualenv
venv/

# PyCharm
.idea/
__pycache__/

# MacOS
.DS_Store

# Distribution / packaging
.Python
build/
develop-eggs/
dist/
downloads/
eggs/
.eggs/
lib/
lib64/
parts/
sdist/
var/
wheels/
*.egg-info/
.installed.cfg
*.egg
MANIFEST
```

Where also an eventual virtual environment `venv/` and various other files are excluded.

You should make sure you edit your `.gitignore` to exclude from version control all the files that should not be there.

## Adding Source Code

Packages might be more or less complex, and be organised in few or many folders. There is not a single way to organise your source code. Here the simplest one will be detailed, in which there is a single folder containing the source modules.

Create the `pypackexample` folder within your repo. Inside put your source code (in the example below, two modules):

``` text
pypackexample/
├── .gitignore
├── pypackexample
│   ├── __init__.py
│   ├── mod1.py
│   └── mod2.py
└── README.md
```

Note that `__init__.py` has to be created so that the folder `pypackexample` can be recognised as a package. This can be an empty file. Let's assume that `mod2.py` needs functions defined in `mod1.py`. You should use this import statement in `mod2.py`:

``` python
from pypackexample import mod1
```

This is exactly what is done in the [example repository](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags).

Note that the root repository folder and the package folder inside do not have to have the same name. In fact, you can have multiple package folders in your repo. However, it is most straightforward if they do when your repository provides a single package.

## Adding the `setup.py` File

The package creation is concluded by creating the `setup.py` file in the root of your repository:

``` text
pypackexample/
├── .gitignore
├── pypackexample
│   ├── __init__.py
│   ├── mod1.py
│   └── mod2.py
├── README.md
└── setup.py
```

This is the file that enables to install the package with `pip`. `setup.py` makes use of the `setuptools` module to create the package definition. Complex operations can be done in this file, including building C++ code with python bindings. A minimal example can be found below.

``` python
import setuptools

# Read the README.md file to add it to the long_description attribute below
with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pypackexample",
    version="0.0.1",
    author="Stefano Tronci",
    author_email="stefano.tronci@protonmail.com",
    description="A Python Package Example.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/stefano-tronci/pypackexample",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
```

The `setuptools.find_packages()` function is used to automatically detect the modules exported by this package. These can be alternatively manually provided.

If your package needs dependencies to run, you should add the following argument to `setuptools.setup`:

``` python
    install_requires=[
       'A',
       'B'
    ]
```

In this example `A` and `B` are the package names the dependencies that your package requires. For example, if our package required `numpy`:

``` python
    install_requires=[
       'numpy',
    ]
```

It is possible to specify version ranges. Refer to [this page](https://packaging.python.org/en/latest/discussions/install-requires-vs-requirements/). Much more can be done with `setup.py`, including optional dependencies. For more information about `setup.py` refer to [this guide](https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/).

## Optional: Adding Unit Tests

There isn't a single way to add unit tests to your application. Different sources differ on the best practices. To add directories for test code you can refer to [these guidelines](https://docs.pytest.org/en/stable/explanation/goodpractices.html). For me the most straightforward way is to create your tests as a submodule. Here we create the `tests` submodule within `pypackexample`. This is just a folder that contains an `_init_.py` file and unit tests source code, as follows:

``` text
pypackexample/
├── .gitignore
├── pypackexample
│   ├── __init__.py
│   ├── mod1.py
│   ├── mod2.py
│   └── tests
│       ├── __init__.py
│       ├── test_mod1.py
│       └── test_mod2.py
├── README.md
└── setup.py
```

Again, the `__init__.py` file can be empty. Import statements for your package modules should still be of the type `from pypackexample import mod1`. The [example repository](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags) provides minimal example tests based on [unittest](https://docs.python.org/3/library/unittest.html).

The approach outlined above is suggested in [this tutorial](https://python-packaging.readthedocs.io/en/latest/testing.html). A different simple setup is suggested in [this more official tutorial](https://packaging.python.org/en/latest/tutorials/packaging-projects/). You should weight the pros and cons of each structure and decide the best one for your project. If you are new to unit tests you can read [this excellent tutorial](https://realpython.com/python-testing/).

## Optional: Entry Points

You might want your package to have an entry point. An entry point is a python program within your package that gets executed when a command of your choice is entered in the terminal.

For example, we can add a `main.py` file in `pypackexample`:

``` text
pypackexample/
├── .gitignore
├── pypackexample
│   ├── __init__.py
│   ├── main.py
│   ├── mod1.py
│   ├── mod2.py
│   └── tests
│       ├── __init__.py
│       ├── test_mod1.py
│       └── test_mod2.py
├── README.md
├── requirements.txt
└── setup.py
```

The code in this file should have this structure:

``` python
def main():
  # This is where you will actually do something.
  pass


if __name__ == '__main__':
    main()
```

In the [example repository](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags) the `main` function prints the path to some data files (see @sec-non-code-files).

To add it as an entry point add this argument to the `setup.py` `setuptools.setup` call:

``` python
    entry_points={
        'console_scripts': [
            'pypackexample_main = pypackexample.main:main',
        ],
    },
```

Now, when the environment where `pypackexample` is installed is active, calling `pypackexample_main` in the terminal will run `main()` from `pypackexample/main.py`. You can have as many entry points as you wish, with any name, calling any function from any file.

## Optional: Adding Non-Code Files {#sec-non-code-files}

It could happen that your code needs to make use of non `.py` files. These could be some data files, for example. These files need to be specifically included in the package.

In this example we provide a `.csv` file to the package. One of the simplest locations for the files is in a `data` folder in your package directory. This location makes the file easily accessible for the source.

``` text
pypackexample/
├── .gitignore
├── pypackexample
│   ├── data
│   │   └── data.csv
│   ├── __init__.py
│   ├── mod1.py
│   ├── mod2.py
│   └── tests
│       ├── __init__.py
│       ├── test_mod1.py
│       └── test_mod2.py
├── README.md
└── setup.py
```

The `data` folder is available to the code modules in `pypackexample` relative to their parent folder. This is easiest to do with the [pathlib](https://docs.python.org/3/library/pathlib.html) module. For example, the following function in `mod1.py` from the [example repository](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags) returns the `Path` object to `data.csv`. This can be used to load the file or do any other operation.

``` python
def get_data_path() -> pathlib.Path:
    return pathlib.Path(__file__).parent.joinpath('data', 'data.csv')
```

I think `pathlib` is one of the best ways to deal with paths in Python. It has very useful methods. I therefore recommend it.

We now need to make sure our `data/data.csv` file is included in the package. First, we need to add this argument to `setuptools.setup`:

``` python
    include_package_data=True,
```

Then, we can either use the `package_data` argument or write a `MANIFEST.in` file in the root of the repository. In the following sections we will use them to achieve the same result. In most cases `MANIFEST.in` is going to be easiest to use. Both techniques allow complex options. For more information see [this page](https://setuptools.pypa.io/en/latest/userguide/datafiles.html) as well as [this page](https://python-packaging.readthedocs.io/en/latest/non-code-files.html).

### With `package_data`

Add the following argument to `setuptools.setup`:

``` python
    package_data={'pypackexample': ['data/data.csv']},
```

This is the approach followed in the [example repository](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags), as it avoids introducing another file.

### With `MANIFEST.in`

Create the `MANIFEST.in` file in the root of the repository:

``` text
pypackexample/
├── .gitignore
├── MANIFEST.in
├── pypackexample
│   ├── data
│   │   └── data.csv
│   ├── __init__.py
│   ├── mod1.py
│   ├── mod2.py
│   └── tests
│       ├── __init__.py
│       ├── test_mod1.py
│       └── test_mod2.py
├── README.md
└── setup.py
```

All paths of the data to include must be specified in the `MANIFEST.in` file, with the `include` keyword, relative to the repository root folder. To include all the data in `pypackexample/data` use:

``` text
include pypackexample/data/*
```

## Optional: Hosting Documentation on GitLab Pages

If you document your package code with [docstrings](https://peps.python.org/pep-0257/) it is possible to generate documentation automatically and host it as a static website on GitLab pages. Hosting the documentation on other services, such as GitHub, is most likely very similar (although I haven't tried it).

There are multiple ways to achieve this, by using different docstring formats and documentation generators. Here we will propose a simple example using [reStructuredText](https://docutils.sourceforge.io/rst.html) and [Sphinx](https://www.sphinx-doc.org/en/master/). You can refer to the [example repository](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags) to see how docstrings are used within the code. A good tutorial can be found [here](https://thomas-cokelaer.info/tutorials/sphinx/docstring_python.html).

### Documentation Dependencies

A few additional Python packages are needed in order to develop and build documentation locally. These dependencies are most often not needed to install and use the package, so it is best to specify them with the `extras_require` argument in `setuptools.setup`. The following specifies the needed dependencies for a Sphinx documentation using the `sphinx_rtd_theme`:

``` python
    extras_require = {
       'doc': ['Sphinx', 'sphinx-rtd-theme']
    },
```

These packages must be installed in your environment to complete the following steps.

### Initialise the Sphinx Configuration

Use the `sphinx-quickstart` command from your python environment. To best ensure you are using the correct command, issue `sphinx-quickstart` after you activate your local python environment (if any).

The `sphinx-quickstart` command will ask a set of questions. There are no special requirements and you are free of organising the docs the way you wish. This example will assume that the documentation root path is a special `doc` folder we create in the root of the repository. We will run `sphinx-quickstart` from `doc` and we will separate the `source` and `build` directories. An example of how to achieve this is shown below. From the repository root:

``` commandline
$ mkdir doc && cd doc 

$ sphinx-quickstart
Welcome to the Sphinx 4.5.0 quickstart utility.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you separate
"source" and "build" directories within the root path.
> Separate source and build directories (y/n) [n]: y

The project name will occur in several places in the built documentation.
> Project name: pyPackExample
> Author name(s): Stefano Tronci
> Project release []: 0.0.1

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see
https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
> Project language [en]: en

Creating file /home/crocoduck/Documents/personal_website/pypackexample/doc/source/conf.py.
Creating file /home/crocoduck/Documents/personal_website/pypackexample/doc/source/index.rst.
Creating file /home/crocoduck/Documents/personal_website/pypackexample/doc/Makefile.
Creating file /home/crocoduck/Documents/personal_website/pypackexample/doc/make.bat.

Finished: An initial directory structure has been created.

You should now populate your master file /home/crocoduck/Documents/personal_website/pypackexample/doc/source/index.rst and create other documentation
source files. Use the Makefile to build the docs, like so:
   make builder
where "builder" is one of the supported builders, e.g. html, latex or linkcheck.
```

You should also run the following command:

``` commandline
$ sphinx-apidoc -o source/ ../pypackexample
```

At this point the internal repository structure will look like this:

``` text
pypackexample/
├── doc
│   ├── make.bat
│   ├── Makefile
│   └── source
│       ├── conf.py
│       ├── index.rst
│       ├── _static
│       └── _templates
├── .gitignore
├── pypackexample
│   ├── data
│   │   └── data.csv
│   ├── __init__.py
│   ├── mod1.py
│   ├── mod2.py
│   └── tests
│       ├── __init__.py
│       ├── test_mod1.py
│       └── test_mod2.py
├── README.md
└── setup.py
```

Note that the `doc/build` directory will be excluded by `.gitignore`. This is desired (the various `build` directories that are crated by `pip` and Sphinx are not shown above).

### Update the `conf.py` File

The `conf.py` file in the `doc/source` directory needs to be edited. The following shows the edits required to build the configuration properly.

Import `sphinx_rtd_theme` at the top of the file:

``` python
import sphinx_rtd_theme
```

In the `Path setup` section, add the following paths:

``` python
# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import sys
sys.path.insert(0, os.path.abspath('./'))
sys.path.insert(0, os.path.abspath('../'))
sys.path.insert(0, os.path.abspath('../../'))
sys.path.insert(0, os.path.abspath('../../pypackexample'))
sys.path.insert(0, os.path.abspath('../../../'))
```

In the `General configuration` section, specify the following extensions:

``` python
# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx_rtd_theme',
]
```

In the `Options for HTML output` section, set the `sphinx_rtd_theme` theme:

``` python
# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'
```

And now run this command:

``` commandline
$ sphinx-apidoc -o . ..
```

### Build Locally

To build your static HTML documentation, simply activate your environment (if any) and issue, from the `doc` folder:

``` commandline
$ make clean
$ make html
```

This will produce HTML pages in `doc/build/html`.

The instructions above are based on [this excellent tutorial by Julie Elise](https://betterprogramming.pub/auto-documenting-a-python-project-using-sphinx-8878f9ddc6e9), which contains additional information. Additional info about some commands is available [here](https://stackoverflow.com/a/70947204).

### CI Pipeline to Serve the Static HTML Documentation on GitLab Pages

The contents of the `doc/source` directory can be added to your repository and a CI pipeline can be used to build the documentation and serve it on GitLab pages. To do so, create a `.gitlab-ci.yml` file in the root of your project. A good starting point is the following:

``` yml
image: python:3.9-alpine

before_script:
  - apk update
  - apk upgrade
  - apk add --update build-base libffi-dev openssl-dev python3-dev libsndfile libsndfile-dev

stages:
  - test
  - deploy

test:
  stage: test
  script:
    - python -m pip install --upgrade pip
    - pip install Sphinx sphinx-rtd-theme
    - sphinx-build -b html doc/source/ public/
  only:
    - branches
  except:
    - main
  tags:
    - docker

pages:
  stage: deploy
  script:
    - python -m pip install --upgrade pip
    - pip install Sphinx sphinx-rtd-theme
    - sphinx-build -b html doc/source/ public/
  artifacts:
    paths:
      - public
  only:
    - main
  tags:
    - docker
```

The file above installs Sphinx and `sphinx_rtd_theme` manually. If you have other dependencies for your package it will be easiest to collect them in a `requirements.txt` file in your repository root. Then, add `- pip install -r requirements.txt` step before each `sphinx-build`.

This example uses an Alpine Linux image with python 3.9. If your package has specific python version requirements make sure they are matched.

The `before_script` section makes sure that the appropriate packages needed to build the documentation are installed in the Alpine Linux image. A successful build will require all the needed dependencies for your package. This includes dependencies needed to build sources. The packages that I listed above are not necessary for `pypackexample`. However, I found them to be needed to build projects with `numpy` as a dependency. In case of errors examination of the logs of the CI will show which headers or packages are missing. To help you locating packages you can refer to the [Alpine Linux packages page](https://pkgs.alpinelinux.org/packages). These packages can be installed with the `apk` command.

The following sections are similar and specify the instructions to build the documentation. Note that the documentation is built with the `sphinx-build` command rather than `make`, so to ensure the HTML pages are created in the `public` directory, from which GitLab pages will serve them.

Finally, the URL of your HTML documentation will be available on your GitLab repository home page under `Settings > Pages`. For the [example repository](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags), the url is <https://stefano-tronci.gitlab.io/pypackexample>.

## Installing Your Package

As soon as you are happy with your package you might push it to your repo. From there you and your users can easily install it with `pip`. There are many ways to do it. I put some useful examples below.

### Installing From a Local Copy {#sec-from-local}

If you clone the repository navigate to the root of it and use:

``` commandline
$ pip install .
```

If you are working on the package source and you want the changes to be immediately reflected in the installation use the `-e` option to symlink the sources:

``` commandline
$ pip install -e .
```

### Installing From the Remote

The package can be directly installed with `pip` straight from the remote repository. Below we will use [pyPackExample](https://gitlab.com/stefano-tronci/pypackexample/-/tree/legacy?ref_type=tags) as an example.

#### With SSH

-   Obtain the cloning address from the project homepage: `git@gitlab.com:stefano-tronci/pypackexample.git`.
-   Replace the `:` in the address with `/`: `git@gitlab.com/stefano-tronci/pypackexample.git`.
-   Prepend `git+ssh://` to it: `git+ssh://git@gitlab.com/stefano-tronci/pypackexample.git`. This is the argument to pass to `pip`.

For example:

``` commandline
$ pip install git+ssh://git@gitlab.com/stefano-tronci/pypackexample.git
```

#### With HTTPS

-   Obtain the cloning address from the project homepage: `https://gitlab.com/stefano-tronci/pypackexample.git`.
-   Prepend `git+` to it: `git+https://gitlab.com/stefano-tronci/pypackexample.git`. This is the argument to pass to `pip`.

For example:

``` commandline
$ pip install git+https://gitlab.com/stefano-tronci/pypackexample.git
```

#### Tips and Tricks

We can do much more than simply installing. I will present a bunch of examples using the HTTPS protocol. The same can be done with SSH.

First, we can tell the package name to `pip`. In our case:

``` commandline
$ pip install pypackexample@git+https://gitlab.com/stefano-tronci/pypackexample.git
```

Remember that the package name is defined in [`setup.py`](https://gitlab.com/stefano-tronci/pypackexample/-/blob/main/setup.py#L8). This might seem useless, but it is actually needed for optional dependencies (see below). Also, it makes things more readable when we are specifying dependencies (see @sec-as-dept).

If you want to install your package from another branch, for example `devel`, just append `@devel` to the argument:

``` commandline
$ pip install pypackexample@git+https://gitlab.com/stefano-tronci/pypackexample.git@devel
```

Note that the same applies to any tag or commit. Just use the tag or the commit hash after `@`.

If you wish to install some optional dependencies from the `extras_require` list it in brackets after the package name. For example, to install the `doc` optional dependencies (together with the normal ones):

``` commandline
$ pip install pypackexample[doc]@git+https://gitlab.com/stefano-tronci/pypackexample.git
```

While developing it could happen that you need to reinstall a modified package, but the `version` specified in `setup.py` is not bumped up. In this case you should first uninstall your package, and then install it again. Or, use the `--force-reinstall` and `--no-cache-dir` option, so that the package is reinstalled from the remote (and not from the cache). For example:

``` commandline
$ pip install --force-reinstall --no-cache-dir pypackexample@git+https://gitlab.com/stefano-tronci/pypackexample.git
```

If your package builds something from source (for example some [pybind11](https://pybind11.readthedocs.io/en/stable/index.html) bindings) then you want to force `pip` to also recompile it. This is achieved by the `--no-binary` option followed by the name of the package. For example:

``` commandline
$ pip install --force-reinstall --no-cache-dir --no-binary :pypackexample: pypackexample@git+https://gitlab.com/stefano-tronci/pypackexample.git
```

You might use also the `--verbose` option to check what is going on in details.

Most often you do not need to install the package while developing it. In my experience [PyCharm](https://www.jetbrains.com/pycharm/) will work nicely without installing by using the same `import` statement you would use to create a functional package.

If you need to install the package (maybe because you are building `pybind11` bindings) install from your local copy with symlinks (see @sec-from-local). If you are building bindings use the `--no-binary` option as well if your bindings or underlying code change. Your `pip` command will look something like this:

``` commandline
$ pip install -e --no-binary :pypackexample: .
```

### Specifying Your Package as a Dependency {#sec-as-dept}

Specifying your package as a dependency of other Python packages and programs is easy. The arguments that we used above for `pip` can just go in a `requirements.txt` file or in `install_requires` or `extras_require` arguments.

For example, a `requirements.txt` file could look like this:

``` text
numpy
scipy
matplotlib
pypackexample@git+https://gitlab.com/stefano-tronci/pypackexample.git@devel
```

And a `install_requires` could instead look like this:

``` python
    install_requires=[
        'numpy',
        'scipy',
        'matplotlib',
        'pypackexample@git+https://gitlab.com/stefano-tronci/pypackexample.git@devel',
    ]
```

Your can use both HTTPS and SSH when listing your package as a dependency. Normally it is best to use HTTPS to specify dependencies, so that your users do not need to setup SSH with the repository hosting provider. However, if your package is internally distributed within your organisation it might be best to use SSH. Especially if it depends on other internally distributed packages. This way your co-workers will not have to type the password multiple times to install your package.

## Conclusion

In this article I summarised how to create a Python package from scratch. This package can be hosted on any Git repository and installed with `pip`. This hosting solution is very versatile and is especially well suited for organisations. This is also very useful for open source projects. Whilst not covered in this article it is possible to go ahead and submit such packages to PyPI.

## References

I used and linked many references throughout the article. They are all collected here as well.

### General

-   [Getting Started With `setuptools` and `setup.py`](https://pythonhosted.org/an_example_pypi_project/setuptools.html)
-   [Python Packaging User Guide](https://packaging.python.org/)
-   [How To Package Your Python Code](https://python-packaging.readthedocs.io/en/latest/index.html)
-   [setuptools](https://setuptools.readthedocs.io/en/latest/index.html)
-   [Adding Non-Code Files](https://python-packaging.readthedocs.io/en/latest/non-code-files.html)

### Git Hosting

-   [How to use GitHub as a PyPi server](https://www.freecodecamp.org/news/how-to-use-github-as-a-pypi-server-1c3b0d07db2/)
-   [How to create a PIP package hosted on private Github repo](https://dev.to/rf_schubert/how-to-create-a-pip-package-and-host-on-private-github-repo-58pa)

### Integration Practices

-   [Getting Started With Testing in Python](https://realpython.com/python-testing/)
-   [Good Integration Practices](https://docs.pytest.org/en/stable/goodpractices.html)

### Documentation

-   [Auto-Documenting a Python Project Using Sphinx](https://betterprogramming.pub/auto-documenting-a-python-project-using-sphinx-8878f9ddc6e9)
-   [Example Sphinx Repository](https://gitlab.com/pages/sphinx)
-   [Solution to Common Sphinx Issues](https://stackoverflow.com/questions/13838368/no-generation-of-the-module-index-modindex-when-using-sphinx/70947204#70947204)
-   [Sphinx](https://www.sphinx-doc.org/en/master/)
-   [Example on how to document your Python docstrings](https://thomas-cokelaer.info/tutorials/sphinx/docstring_python.html)
-   [Markup Syntax and Parser Component of Docutils](https://docutils.sourceforge.io/rst.html)

### Non-Code Files

-   [Data Files Support](https://setuptools.pypa.io/en/latest/userguide/datafiles.html)

*The Featured Picture is From The [Python Project](https://www.python.org/community/logos/).*