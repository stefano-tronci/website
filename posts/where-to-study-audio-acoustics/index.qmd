---
title: "Where to Study Audio Acoustics"
author: "Stefano Tronci"
date: "2018-05-27"
categories: [Education]
image: "thumbnail.jpg"
---

![](thumbnail.jpg)

When I was completing my Bachelor degree in Physics, I started looking around to see whether I could find Universities offering courses in Acoustics, but with (mainly) audio applications in mind. Whilst many engineering departments might offer Environmental Acoustics programs, it is harder to find the ones about Audio Acoustics. I though it could be useful to summarise in this post the main options I found, both while I was looking around and afterwards.

This list is far from exhaustive, and mainly from my memory. It also only considers places where I was considering to move, so perhaps there are many more Universities around the world where to study Audio Acoustics.

# Europe

[Salford University](https://www.salford.ac.uk/): This is the university were I earned my [Master Degree in Audio Acoustics]({{< ref "/education.md" >}}). At the time I attended (2014-2015) the University was offering few different programs: 2 Masters in acoustics, one audio oriented and one with a more environmentally focused curriculum, plus two similarly oriented Bachelor degrees and, if I remember correctly, a program of Physics with Acoustics. See the [course search](https://search2.salford.ac.uk/s/search.html?query=acoustics&collection=courses&btn-submit=) for an up to date list. I am overall happy with the experience I had at Salford: it allowed me to meet many like minded people, the school staff was always kind and helpful, and overall it was a smooth experience. However, I feel like the courses did not go as deep as I would have liked, especially those about DSP and transducer design. The reason why I attended this one instead of the others? Well, by the time I could think about enrolling to a program, this one only had open applications.

[Southampton](https://www.southampton.ac.uk/): I found out about this other University while at Salford, as its name would often pop up. This University offers various courses in Acoustics and Music.

[Edinburgh](https://www.ed.ac.uk/): This looks like a nice option to me. Edinburgh University appears to offer both undergraduate and graduate Acoustics courses. Refer to course searching tools and [this page](http://www.acoustics.ed.ac.uk/).

[Aalto](http://www.aalto.fi/en/): one of the most active universities in the field, with both undergraduate and postgraduate courses. Refer to [this page](http://spa.aalto.fi/en/) or just do a simple [google search](https://www.google.com/search?hl=en&q=Aalto%20acoustics).

The [Technical University of Denmark](http://www.dtu.dk/english)offers courses in acoustics. I was interested in the [Engineering Acoustics](http://www.dtu.dk/english/education/msc/programmes/engineering_acoustics) Master course when I was choosing where to go for the Master, but applications were closed. The [search tool](http://www.dtu.dk/english/resultat?qt=NetmesterSearch&fr=1&sw=acoustics#tabs) returns few more results.

[Technische Universität Berlin](http://www.tu-berlin.de/menue/home/): I found about this one while looking at the background of few speakers at a conference (one the [Linux Audio Conferences](http://linuxaudio.org/lac.html) I believe) see the [Audio Communication Group](http://www.ak.tu-berlin.de/menue/fachgebiet_audiokommunikation/parameter/en/) and this interesting page with [many theses](http://www.ak.tu-berlin.de/menue/master_theses/parameter/en/).

Whilst not an University, [IRCAM](https://www.ircam.fr/) is worth mentioning. IRCAM appears to host research facilities used in cooperation with the [Sorbonne University](https://sciences.sorbonne-universite.fr), which also offers acoustics courses (for example [this](https://sciences.sorbonne-universite.fr/formation-sciences/masters/master-de-mecanique/parcours-acoustique)).

On a similar note to IRCAM, it is possible to mention [STEIM](https://steim.org/), which actively partners with universities in order to offer various courses, although the focus of STEIM activities appears to be less technical.

# USA

[Stanford University](https://www.stanford.edu/): Interesting courses are offered by [CCRMA](https://ccrma.stanford.edu/). This department is extremely influential, especially in the realm of open source, Linux and culture dissemination. See [Planet CCRMA](http://ccrma.stanford.edu/planetccrma/software/), [Julius Orion Smith](https://ccrma.stanford.edu/~jos/) and [Romain Michon](https://ccrma.stanford.edu/~rmichon/) pages for few examples.

# Australia

The [University of New South Whales](https://www.unsw.edu.au/) appears to heave an [acoustics department](https://www.physics.unsw.edu.au/research/acoustics/acoustics) with special interest in musical acoustics. It isn't clear if they offer courses, though.

# How to Find More?

Beside endless time on search engines, it is good to look at conferences and conventions on Acoustics and Audio: just track the Universities from where the speakers are from. For example, you might look at [Linux Audio Conferences](http://linuxaudio.org/lac.html) and [AES](http://www.aes.org/).
